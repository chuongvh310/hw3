const express = require('express');

const app = express();
app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});
app.get('/login', (req, res) => {
  res.sendFile(__dirname + '/login.html');
});
app.get('/register', (req, res) => {
  res.sendFile(__dirname + '/register.html');
});
const port = +process.env.PORT || 9999; // if- else
app.listen(port, (err) => {
  if (err) {
    return console.log(err);
  }
  return console.log(`server listening ${port}`);
});